# README #

Detta är ett repo för elever som studerar till systemutvecklare i Östersund. Syftet med detta är att lära sig hur man arbetar med versionshantering. 

### Elever - Hur man gör? ###
* Gör en [**Fork**](https://bitbucket.org/java063/ya-elever/fork) av repot.
* Lägg till din remote av repot från ditt konto via cmd på din lokala dator och hämta innehållet.
* Skapa en ny **Branch** lokalt på din dator för detta repo som du arbetar med.
* Lägg till ditt namn och bitbucket username på en ny rad i filen elever.txt via ett terminal kommando.
* Gör en **Stage** och sedan **Commit** lokalt på din dator.
* Synka ditt Forkade repo med detta repo innan nästa steg.
* Utför en **Merge** med din lokala master och branchen du arbetat med.
* Gör en **Push** så repot uppdateras på ditt konto.
* Utför en [**Create pull request**](https://bitbucket.org/java063/ya-elever/pull-request/new) så ditt namn läggs till i master på detta repo.

### Admin/Lärare - Hur gör man? ###

* Under[ Navigation -> Pull requests](https://bitbucket.org/java063/ya-elever/pull-requests):
* Godkänn elevernas **Pull request** så deras namn läggs till i repot.

